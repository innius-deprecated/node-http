import {Http, Options} from "./../http";

import {Observable, Observer} from "@reactivex/rxjs";

import {request as http_request, response} from "http";
import {request as https_request} from "https";
import {UriParser} from "../lib/uriparser";
import {ClientRequest} from "http";

export class NetworkHttp implements Http {
    get(uri: string, opts?: Options): Observable<any> {
        return Observable
            .create((observer: Observer<string>) => {
                if (!opts) {
                    opts = {};
                }
                let parsed_uri = new UriParser(uri);

                let cb = (response: response) => {
                    var str = "";
                    if (opts.expectedStatus && Array.isArray(opts.expectedStatus) && opts.expectedStatus.length > 0 && opts.expectedStatus.indexOf(response.statusCode)==-1) {
                        observer.error(new Error(`Status ${response.statusCode} returned while requesting ${parsed_uri.host}:${parsed_uri.port}${parsed_uri.path}.`));
                    }
                    response.on("data", (chunk) => {
                        str += chunk;
                    });

                    response.on("end", () => {
                        observer.next(str);
                        observer.complete();
                    });

                    response.on("error", (e) => {
                        observer.error(e);
                    });
                };

                let req: ClientRequest;

                if (parsed_uri.isHttp) {
                    req = http_request({
                        host: parsed_uri.host,
                        port: parsed_uri.port,
                        path: parsed_uri.path
                    }, cb);
                } else {
                    req = https_request({
                        host: parsed_uri.host,
                        port: parsed_uri.port,
                        path: parsed_uri.path
                    }, cb);
                }

                req.on("error", (e) => {
                    observer.error(e);
                });

                if (opts.timeout) {
                    req.setTimeout(opts.timeout, () => {
                        observer.error(new Error(`A timeout occurred while requesting ${parsed_uri.host}:${parsed_uri.port}${parsed_uri.path}.`));
                    });
                }

                req.end();
            });
    }
}
