import {assert} from "chai";
import {UriParser} from "./uriparser";

describe("#network", () => {
    describe("of http://foo.bar.example.com:8080/abc/xyz", () => {
        let uri = new UriParser("http://foo.bar.example.com:8080/abc/xyz");
        it("should get the host", () => {
            assert.equal(uri.host, "foo.bar.example.com");
        });
        it("should get the port", () => {
            assert.equal(uri.port, 8080);
        });
        it("should get the path", () => {
            assert.equal(uri.path, "/abc/xyz");
        });
    });
    describe("of https://example.com/xyz", () => {
        let uri = new UriParser("https://example.com/xyz");
        it("should get the host", () => {
            assert.equal(uri.host, "example.com");
        });
        it("should get the port", () => {
            assert.equal(uri.port, 443);
        });
        it("should get the path", () => {
            assert.equal(uri.path, "/xyz");
        });
    });
    describe("of http://example.com", () => {
        let uri = new UriParser("http://example.com");
        it("should get the host", () => {
            assert.equal(uri.host, "example.com");
        });
        it("should get the port", () => {
            assert.equal(uri.port, 80);
        });
        it("should get the path", () => {
            assert.equal(uri.path, "/");
        });
    });
    describe("of http://example.com:1234", () => {
        let uri = new UriParser("http://example.com:1234");
        it("should get the host", () => {
            assert.equal(uri.host, "example.com");
        });
        it("should get the port", () => {
            assert.equal(uri.port, 1234);
        });
        it("should get the path", () => {
            assert.equal(uri.path, "/");
        });
    });
    describe("of https://example.com", () => {
        let uri = new UriParser("https://example.com:1234");
        it("should be example.com", () => {
            assert.equal(uri.host, "example.com");
        });
        it("should be https", () => {
            assert.equal(uri.isHttp, false);
        });
    });
    describe("of http://example.com", () => {
        let uri = new UriParser("http://example.com:1234");
        it("should be example.com", () => {
            assert.equal(uri.host, "example.com");
        });
        it("should be https", () => {
            assert.equal(uri.isHttp, true);
        });
    });
});
