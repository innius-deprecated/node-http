import {parse} from "url";

export class UriParser {
    public host: string;
    public port: number;
    public path: string;
    public isHttp: boolean;

    constructor(uri: string) {
        let u = parse(uri);
        this.host = u.host;
        this.port = Number(u.port);
        this.path = u.path;

        if (this.host.indexOf(":") !== -1) {
            let splitted = this.host.split(":");
            this.host = splitted[0];
            this.port = Number(splitted[1]);
        }

        if (u.protocol === "http:") {
            this.isHttp = true;
            if (this.port === 0) {
                this.port = 80;
            }
        } else {
            this.isHttp = false;
            if (this.port === 0) {
                this.port = 443;
            }
        }
    }
}
