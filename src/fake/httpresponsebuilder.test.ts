import {assert} from "chai";

import {HttpResponseBuilderImpl} from "./httpresponsebuilder";

describe("#HttpResponseBuilder", () => {
    it("should be able execute a request", (done) => {
        let resp = new HttpResponseBuilderImpl(undefined, "");
        resp.withBody("abc");
        resp.invoke()
            .subscribe(
            (x) => assert.equal(x, "abc"),
            (e) => done(e),
            done
            );
    });
    it("should be able to set a delay", (done) => {
        let resp = new HttpResponseBuilderImpl(undefined, "");
        resp.withBody("abc");
        resp.withDelay(100);
        resp.invoke().timeout(50)
            .subscribe(
            (x) => done(new Error("Should timeout instead of next()")),
            (e) => done(),
            () => done(new Error("Should timeout instead of complete()"))
            );
    });
    it("should retain the response format as is", (done) => {
        let input = [
            "a",
            "b",
            "c"
        ];
        let resp = new HttpResponseBuilderImpl(undefined, "");
        resp.withBody(input);
        resp.invoke()
            .subscribe(
            (x) => {
                assert.deepEqual(x, input);
            },
            (e) => done(e),
            () => done()
            );
    });
    it("should stringify the body", (done) => {
        let input = [
            "a",
            "b",
            "c"
        ];
        let resp = new HttpResponseBuilderImpl(undefined, "");
        resp.withStringifiedBody(input);
        resp.invoke()
            .subscribe(
            (x) => {
                assert.deepEqual(x, "[\"a\",\"b\",\"c\"]");
            },
            (e) => done(e),
            () => done()
            );
    });
});
