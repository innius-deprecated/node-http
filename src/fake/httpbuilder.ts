import {HttpBuilder, HttpResponseBuilder} from "./builder";
import {Http} from "../http";
import {HttpResponseBuilderImpl} from "./httpresponsebuilder";
import {FakeHttp} from "./fake";

/**
 * Create a new HttpBuilder that can be used for testing.
 * @returns {HttpBuilder}
 */
export function newHttpBuilder(): HttpBuilder {
    return new HttpBuilderImpl();
}

export class HttpBuilderImpl implements HttpBuilder {
    private responses: HttpResponseBuilderImpl[];

    constructor() {
        this.responses = [];
    }


    build(): Http {
        return new FakeHttp(this);
    }

    withGet(uri?: string): HttpResponseBuilder {
        if (!uri) {
            uri = "";
        }
        return new HttpResponseBuilderImpl(this, uri);
    }

    // internal usage only

    addResponse(response: HttpResponseBuilderImpl): void {
        this.responses.push(response);
        return;
    }

    match(uri: string): HttpResponseBuilderImpl {
        let options = this.responses.filter((r: HttpResponseBuilderImpl) => {
            return uri.startsWith(r.uri());
        });
        if (options.length > 0) {
            return options[0];
        }
        return undefined;
    }

}
