import {assert} from "chai";

import {newHttpBuilder} from "./httpbuilder";

import {Observable} from "@reactivex/rxjs";

describe("#HttpBuilder", () => {
    it("should be able to get a payload", (done) => {
        let http = newHttpBuilder()
            .withGet("/abc/def")
            .withBody("abc")
            .buildResponse()
            .build();
        http.get("/abc/def")
            .subscribe(
            (x) => assert.equal(x, "abc"),
            (e) => done(e),
            done
            );
    });
    it("should not be able to find a payload", (done) => {
        let http = newHttpBuilder()
            .withGet("/abc/def")
            .withBody("abc")
            .buildResponse()
            .build();
        http.get("/xyz")
            .subscribe(
            (x) => done(new Error("next should not be called")),
            (e) => done(),
            () => done(new Error("complete may not be called"))
            );
    });
    it("should be able to get a payload without specifying a route.", (done) => {
        let http = newHttpBuilder()
            .withGet()
            .withBody("abc")
            .buildResponse()
            .build();
        http.get("/abc/def")
            .subscribe(
            (x) => assert.equal(x, "abc"),
            (e) => done(e),
            done
            );
    });
    it("should allow only a single invocation", (done) => {
        let http = newHttpBuilder()
            .withGet("/abc/def")
            .withBody("abc")
            .times(1)
            .buildResponse()
            .build();

        var cnt = 0;

        Observable.from([1, 2]).flatMap((x) => {
            return http.get("/abc/def");
        }).subscribe(
            (x) => {
                assert.equal(x, "abc");
                cnt++;
            },
            (e) => {
                assert.equal(1, cnt);
                done()
            },
            () => done(new Error("complete may not be called"))
            );
    });
    it("should invoke callbacks", (done) => {
        var invocation_sum = 1;

        let http = newHttpBuilder()
            .withGet("/abc/def")
            .withBody("abc")
            .withCallback((invocation_count) => {
                invocation_sum *= invocation_count;
            })
            .withCallback((invocation_count) => {
                invocation_sum *= invocation_count;
            })
            .times(1)
            .buildResponse()
            .build();

        Observable.from([1, 2]).flatMap((x) => {
            return http.get("/abc/def");
        }).subscribe(
            (x) => {
                assert.equal(x, "abc");
            },
            (e) => {
                assert.equal(4, invocation_sum); // 1 * 1 * 1 * 2 * 2
                done()
            },
            () => done(new Error("complete may not be called"))
            );
    });
});
