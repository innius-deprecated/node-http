import {HttpResponseBuilder, HttpBuilder} from "./builder";
import {HttpBuilderImpl} from "./httpbuilder";

import {Observable} from "@reactivex/rxjs";

export class HttpResponseBuilderImpl implements HttpResponseBuilder {
    private payload: Observable<any>;
    private timeout: number;
    private counter: number;
    private cbs: any[];
    private invocation_count: number;

    constructor(private parent: HttpBuilderImpl, private _uri: string) {
        this.payload = Observable.empty();
        this.timeout = 0;
        this.counter = 0;
        this.cbs = [];
        this.invocation_count = 0;
    }

    // Internal stuff
    invoke(): Observable<any> {
        var that = this;
        var asserted = false;

        var assert = () => {
            if (!asserted) {
                asserted = true;
                that.invocation_count++;

                that.cbs.forEach((cb) => {
                    cb(that.invocation_count);
                });

                let limited = that.counter !== 0;
                let limit_reached = that.invocation_count > that.counter;

                if (limited && limit_reached) {
                    throw new Error(`Too many invocations of ${that.uri}; had ${that.invocation_count - 1}, allowed ${that.counter}`);
                }
            }
        };

        let obs = this.payload.do(assert, assert, assert);

        if (this.timeout !== 0) {
            obs = obs.delay(this.timeout);
        }
        return obs;
    }

    uri(): string {
        return this._uri;
    }

    // Builder iface:

    never(): HttpResponseBuilder {
        this.payload = Observable.never();
        return this;
    }

    withBody(body: any): HttpResponseBuilder {
        this.payload = Observable.of(body);
        return this;
    }

    withStringifiedBody(body: any): HttpResponseBuilder {
        this.payload = Observable.of(JSON.stringify(body));
        return this;
    }

    withError(err: any): HttpResponseBuilder {
        this.payload = Observable.throw(err);
        return this;
    }

    withDelay(timeout: number): HttpResponseBuilder {
        this.timeout = timeout;
        return this;
    }

    withCallback(callback: (num: number) => void): HttpResponseBuilder {
        this.cbs.push(callback);
        return this;
    }

    times(amount: number): HttpResponseBuilder {
        this.counter = amount;
        return this;
    }

    buildResponse(): HttpBuilder {
        this.parent.addResponse(this);
        return this.parent;
    }
}
