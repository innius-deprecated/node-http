import {Http, Options} from "../http";
import {Observable} from "@reactivex/rxjs";
import {HttpBuilderImpl} from "./httpbuilder";

export class FakeHttp implements Http {
    constructor(private builder: HttpBuilderImpl) {

    }

    get(uri: string, opts?: Options): Observable<any> {
        let route = this.builder.match(uri);
        if (route) {
            return route.invoke();
        } else {
            return Observable.throw(new Error("No routes matched"));
        }
    }

}
