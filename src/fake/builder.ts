import {Http} from "../http";

export interface HttpBuilder {
    /**
     * Build a Http provider from this builder. It cannot be modified anymore.
     * @returns Http An http provider.
     */
    build(): Http;

    /**
     * Add a request to this HttpBuilder. If an URI is provided, that will be used. If none given, 
     * it will assume that this response can always be given.
     * @param uri The uri to send a response on.
     */
    withGet(uri?: string): HttpResponseBuilder;
}

export interface HttpResponseBuilder {
    /**
     * No response should be given at all.
     */
    never(): HttpResponseBuilder;
    /**
     * Respond with a given body.
     * @param body
     */
    withBody(body: any): HttpResponseBuilder;
    /**
     * Respond with a given body, calling JSON.stringify() on it beforehand.
     * @param body
     */
    withStringifiedBody(body: any): HttpResponseBuilder;
    /**
     * Responde with a given error.
     * @param err
     */
    withError(err: any): HttpResponseBuilder;
    /**
     * This request should be delayed.
     * @param timeout The timeout in milliseconds.
     */
    withDelay(timeout: number): HttpResponseBuilder;
    /**
     * Register a callback that will be invoked every time this http response is served. 
     * @param callback
     */
    withCallback(callback: (num: number) => void): HttpResponseBuilder;
    /**
     * How many times may this URI be served? If the limit is reached, an error
     * is thrown instead of being provided with the configured value.
     * @param amount
     */
    times(amount: number): HttpResponseBuilder;
    /**
     * Build this http response 
     */
    buildResponse(): HttpBuilder;
}
