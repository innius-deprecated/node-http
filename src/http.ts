import {Observable} from "@reactivex/rxjs";

export interface Http {
    /**
     * Execute a get request
     * @param uri The uri to get.
     */
    get(uri: string, opts?: Options): Observable<any>;
}

export interface Options {
    timeout?: number;
    expectedStatus?: number[];
}
